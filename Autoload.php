<?php

namespace appage\taggle;

use Yii;

class Autoload extends \yii\base\Widget
{
    public function run()
    {
        Yii::setAlias('@appage/taggle', '@vendor/appage/yii2-taggle');
    }
}
