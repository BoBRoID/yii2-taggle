<?php
namespace appage\taggle\assets;

use yii\web\AssetBundle;

class TagsCustomAsset extends AssetBundle
{
    public $sourcePath = '@appage/taggle/assets/css';
    public $baseUrl = '@web';
    public $css = [
        'taggle.css'
    ];
}
