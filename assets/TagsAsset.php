<?php
namespace appage\taggle\assets;

use yii\web\AssetBundle;

class TagsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/taggle/src';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'taggle.js',
    ];
    public $depends = [
        'appage\taggle\assets\TagsCustomAsset',
    ];
}
