Yii2 Widget for Taggle
======================
Yii2 Widget for okcoker/taggle.js

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist appage/yii2-taggle "*"
```

or add

```
"appage/yii2-taggle": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \appage\taggle\widgets\TagsWidget::widget([
    'clientOptions' => [
        'tags' => ['These', 'are', 'prefilled', 'tags'],
    ],
    'name' => 'example[]',
]) ?>
```

or with model: (add two blocks together)

```php
// If tags list empty, this will save empty array
<?= $form->field($model, 'tagsList')->hiddenInput([
    'name' => $model->formName() . '[tagsList][]',
    'value' => '',
])->label(false) ?>

// If not empty - this will add tags
<?= $form->field($model, 'tagsList[]')->widget(\appage\taggle\widgets\TagsWidget::className(), [
    'clientOptions' => [
        'hiddenInputName' => $model->formName() . '[tagsList][]',
        'tags' => ($model->tagsList ? $model->tagsList : []),
    ],
]) ?>
```

If you store tags list like a string with comma separator you can use our behavior for model:
```php
public function behaviors()
{
    return [
        'tagsList' => [
            'class' => 'appage\taggle\behaviors\TagsBehavior',
            'owner' => $this,
        ],
    ];
}
```

And add _**safe**_ rule for `tagsList`

**Note**: don't forget to add _$hiddenInputName_ param

To show tags you can use TagsListWidget:
```php
echo TagsListWidget::widget([
    'tagsList' => $model->tagsList,
]);
```

For more info you can see [okcoker/taggle.js](https://github.com/okcoker/taggle.js) documentation. 