<?php
namespace appage\taggle\behaviors;

use yii\base\Behavior;

class TagsBehavior extends Behavior
{
    /**
     * @var string
     */
    public $tagsAttr = 'tags';

    /**
     * @return array
     */
    public function getTagsList()
    {
        if ($this->owner->{$this->tagsAttr} != '') {
            $result = explode(',', $this->owner->{$this->tagsAttr});
        } else {
            $result = [];
        }
        return $result;
    }

    /**
     * @param array $tagsArray
     */
    public function setTagsList(array $tagsArray)
    {
        foreach ($tagsArray as $key => $tag) {
            if ($tag == '') {
                unset($tagsArray[$key]);
            }
        }

        $this->owner->{$this->tagsAttr} = join(',', $tagsArray);
    }
}
