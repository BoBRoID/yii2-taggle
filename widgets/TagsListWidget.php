<?php

namespace appage\taggle\widgets;

use yii\base\Widget;

class TagsListWidget extends Widget
{
    /**
     * @var array
     */
    public $tagsList = [];

    public function run()
    {
        return $this->render('tags', ['tagsList' => $this->tagsList]);
    }
}
