<?php

namespace appage\taggle\widgets;

use appage\taggle\assets\TagsAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\InputWidget;

class TagsWidget extends InputWidget
{
    const PLUGIN_NAME = 'tags_input';

    public $clientOptions = [];

    public $options = [];

    private $_hashVar;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerClientScript();

        /* Creates fake block */
        echo Html::tag('div', '', [
            'id' => $this->_hashVar,
            'class' => 'taggle-block clearfix',
        ]);
    }

    /**
     * Generates a hashed variable to store the plugin `clientOptions`. Helps in reusing the variable for similar
     * options passed for other widgets on the same page.
     *
     * @param View $view the view instance
     */
    protected function hashPluginOptions($view)
    {
        $encOptions = empty($this->clientOptions) ? '{}' : Json::encode($this->clientOptions);
        $this->_hashVar = self::PLUGIN_NAME . '_' . hash('crc32', $encOptions);
        $view->registerJs("var {$this->_hashVar} = {$encOptions};\n", View::POS_HEAD);
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        $js = '';
        $view = $this->getView();
        $this->clientOptions = array_merge($this->clientOptions, [
            'duplicateTagClass' => 'bounce',
        ]);
        $this->hashPluginOptions($view);

        $js .= "var elem_" . $this->_hashVar . " = new Taggle(document.querySelectorAll('#" . $this->_hashVar . "')[0], " . $this->_hashVar . ");";
        TagsAsset::register($view);
        $view->registerJs($js);
    }
}