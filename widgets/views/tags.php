<?php
/**
 * @var $this \yii\web\View
 * @var $tagsList []
 */

use yii\helpers\Html;

\appage\taggle\assets\TagsAsset::register($this);

if (!empty($tagsList)) :
    echo Html::beginTag('div', ['class' => 'article-tags']);
    echo 'Tags: ';
    foreach ($tagsList as $tag) :
        echo Html::tag('div', $tag, ['class' => 'article-tag']);
    endforeach;
    echo Html::endTag('div');
endif;
